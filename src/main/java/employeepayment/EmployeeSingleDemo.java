package employeepayment;

import java.util.Calendar;

/**
 *
 * @author mitpa
 */
public class EmployeeSingleDemo 
{
    public static void main(String[]args)
    {
        Calendar dateJoin = Calendar.getInstance();
        dateJoin.set( 2021, 0 , 22);
        
        Employee employee = new Employee("0001", dateJoin.getTime() , 40000);
        
        EmployeeTool tool = new EmployeeTool();
        
        System.out.println("IS PROMOTION DUE " +tool.isPromotionDueThisYear(employee, true));
        System.out.println("TAXES FOR THIS YEAR " +tool.calcIncomeTaxForCurrentYear(employee, 0.28));
       
    }
}
