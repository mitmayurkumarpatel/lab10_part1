package employeepayment;

import java.util.Date;

/**
 *
 * @author mitpa
 */
public class Employee 
{
    private String employeeId;
    private Date dateOfJoining;
    private double Salary;
    
    //no arg constructor
    public Employee(){}

    //arg constructor
    public Employee(String employeeId, Date dateOfJoining, double Salary) {
        this.employeeId = employeeId;
        this.dateOfJoining = dateOfJoining;
        this.Salary = Salary;
    }

    //getter(s)
    public Date getEmployeeDateOfJoining()
    {
        return dateOfJoining;
    }
      public String getEmployeeId()
    {
        return employeeId;
    }
      public double getEmployeeSalary()
      {
          return Salary;
      }

    
 }
