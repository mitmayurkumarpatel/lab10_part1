package employeepayment;

import employeepayment.Employee;
import java.util.Calendar;

public class EmployeeTool 
{
    public boolean isPromotionDueThisYear(Employee employee, boolean goodPerformance)
    {
        Calendar dateJoined = Calendar.getInstance();
        dateJoined.setTime(employee.getEmployeeDateOfJoining());
        dateJoined.add(Calendar.YEAR,1);
        
        Calendar today = Calendar.getInstance();
        
        //comparing todays date with date of joining
        return today.after( dateJoined ) && goodPerformance;  
    }
    public double calcIncomeTaxForCurrentYear( Employee employee, double taxPercentage)
    {
        return employee.getEmployeeSalary() * taxPercentage;
    }
    
}